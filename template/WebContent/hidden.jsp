<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<title>HiddenPage!!!!</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<style type="text/css">
		#button {
            display: inline-block;
            align: left;
		}

		h3 {
			text-decoration: underline;
		}

		h5 {
			color: red;
		}

		#description {
			color: red;
		}
	</style>
	<script type="text/javascript">
		function submitAction(url) {
			$('form').attr('action', url);
			$('form').submit();
		}
	</script>
</head>
<body>
<h1>隠しページへようこそ！</h1>


<!-- カートの中身表示 -->
<form action="GoHomeAction">
	<s:if test="#session.itemList != null">
		<table border="1">
		    <tr>
		        <th>id</th>
		        <th>アイテム名</th>
		        <th>価格</th>
		    </tr>
		<s:iterator value="itemList">
		    <tr>
		        <td><s:property value="itemId" /></td>
			    <td><s:property value="itemName" /></td>
			    <td><s:property value="itemPrice" /></td>
		    </tr>
		</s:iterator>
		</table>
	</s:if>
	<s:else>
		<h2>カートの中身はありません。</h2>
	</s:else>
	<input type="submit" value="ていや"/>
</form>


<!-- 商品一覧表示 -->
<form action="GoHomeAction" method="POST">
    <input type="hidden" name="list" value="<s:property value='list' />" />
    <!-- <input type="hidden" name="list" value="${list}" /> -->
    <table border="1">
        <tr>
	        <th>id</th>
	        <th>アイテム名</th>
	        <th>価格</th>
	        <th>チェック</th>
	    </tr>
			<s:iterator value="list">
			    <tr>
			        <td><s:property value="itemId" /><input type="hidden" name="itemId" value="<s:property value="itemId" />" /></td>
			        <td><s:property value="itemName" /><input type="hidden" name="itemName" value="<s:property value="itemName" />" /></td>
			        <td><s:property value="itemPrice" /><input type="hidden" name="itemPrice" value="<s:property value="itemPrice" />" /></td>
			        <td><input type="checkbox" name="chk" value="<s:property value='itemId' />" /></td>
			    </tr>
		    </s:iterator>
    </table>
		<div id="button">
			<!-- <button type="button" onclick="submitAction('GoHomeAction')">送信</button>
			<button type="button" onclick="submitAction('AddCartAction')">カートに追加</button> -->
			<input type="submit" formaction="GoHomeAction" value="送信" />
			<input type="submit" formaction="AddCartAction" value="カートに追加" />
		</div>
</form>


<!-- カートに追加 -->
	<form action="AddCartAction" method="POST">
		<input type="hidden" name="itemId" value="1" />
		<input type="hidden" name="itemName" value="商品1" />
		<input type="hidden" name="itemPrice" value="100" />
		<button type="submit">商品1をカートに追加</button>
	</form>

	<form action="AddCartAction" method="POST">
		<input type="hidden" name="itemId" value="2" />
		<input type="hidden" name="itemName" value="商品2" />
		<input type="hidden" name="itemPrice" value="200" />
		<button type="submit">商品2をカートに追加</button>
	</form>


<div id="disp">
<!-- form属性の利用 -->
<form method="POST" id="Action">
    <table border="1">
        <tr>
            <th>id</th>
            <th>アイテム名</th>
            <th>価格</th>
            <th>チェック</th>
        </tr>
            <s:iterator value="list">
                <tr>
                    <td><s:property value="itemId" /></td>
                    <td><s:property value="itemName" /></td>
                    <td><s:property value="itemPrice" /></td>
                    <td><input type="checkbox" name="chk" value="<s:property value='itemId' />" /></td>
                </tr>
            </s:iterator>
    </table>
</form>
        <div id="button">
            <!-- <button type="button" onclick="submitAction('GoHomeAction')">送信</button>
            <button type="button" onclick="submitAction('AddCartAction')">カートに追加</button> -->
            <input type="submit" form="Action" formaction="GoHomeAction" value="送信" />
            <input type="submit" form="Action" formaction="AddCartAction"  value="カートに追加" />
        </div>
</div>
${hoge3}
<s:form action="HiddenAction2">
    <s:textfield name="name" />
    <s:property value="name"/>
    <s:submit />
</s:form>
<h1><s:property value="message" /></h1>

<!-- アップロード -->
<h3>画像アップロード</h3>
<form action="HiddenAction" method="POST" enctype="multipart/form-data">
<table>
<tr>
	<td><input type="file" name="myFile" multiple="multiple" accept="image/*" required></td>
</tr>
<!-- <tr>
	<td><input type="file" name="files" multiple="multiple" accept="image/*" required></td>
</tr> -->
</table>
<input type="submit" value="アップロード" />
</form>
<br>
<div id="description">↓Sタグ↓</div>
<form action="HiddenAction" method="POST" enctype="multipart/form-data">
<table>
<s:file name="files" multiple="multiple" accept="image/*" required="required" />
<s:file name="files" multiple="multiple" accept="image/*" required="required" />
<s:submit value="アップロード"/>
</table>
</form>

<form action="HiddenAction" method="POST" enctype="multipart/form-data">
<table>
<s:file name="myFile" multiple="multiple" accept="image/*" required="required" />
<s:submit value="アップロード"/>
</table>
</form>
<h5><s:property value="upmessage" /></h5>

	<script type="text/javascript">
        document.getElementById("disp").style.display="none";
    </script>

</body>
</html>