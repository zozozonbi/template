package com.internousdev.template.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.internousdev.template.dao.HiddenDAO;
import com.internousdev.template.dto.BuyItemDTO;
import com.opensymphony.xwork2.ActionSupport;

public class GoHomeAction extends ActionSupport implements SessionAware {
	private Map<String, Object> session;
	//private String chk;
	private String itemId;
	private String itemName;
	private String itemPrice;
	private ArrayList<Object> list;
	private List<BuyItemDTO> list2;
	private String[] chk;
	private HiddenDAO dao = new HiddenDAO();

	public String execute() {
		System.out.println("chk："+chk);
		System.out.println("id："+itemId);
		System.out.println("itemName："+itemName);
		System.out.println("itemPrice："+itemPrice);
		System.out.println("list："+list);

		try {
			if (chk != null) {
				System.out.println("chk："+chk[0]);
				System.out.println("chk_length："+chk.length);
				list2 = dao.home(chk);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setChk(String[] chk) {
		this.chk = chk;
	}

	public String[] getChk(){
		return this.chk;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemId(){
		return this.itemId;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemName(){
		return this.itemName;
	}

	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}

	public String getItemPrice(){
		return this.itemPrice;
	}

	public void setList(ArrayList<Object> list) {
		this.list = list;
	}

	public ArrayList<Object> getList(){
		return this.list;
	}

	public void setList2(List<BuyItemDTO> list2) {
		this.list2 = list2;
	}

	public List<BuyItemDTO> getList2(){
		return this.list2;
	}
}
