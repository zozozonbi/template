package com.internousdev.template.action;

public class HiddenAction2 {
	private String message;
	private String name = "Fullstack";

	public String execute() {
		message = "Hello" + name;
		System.out.println("ok");
		return "success";
	}
	
	public String getMessage() {
		return message;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
