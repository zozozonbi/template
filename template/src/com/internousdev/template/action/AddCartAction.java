package com.internousdev.template.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.internousdev.template.dao.BuyItemDAO;
import com.internousdev.template.dto.BuyItemDTO;
import com.opensymphony.xwork2.ActionSupport;

public class AddCartAction extends ActionSupport implements SessionAware {

	private Map<String, Object> session;
	private int itemId;
	private String itemName;
	private int itemPrice;
	private ArrayList<Object> list;

	private int[] chk;

	private ArrayList<BuyItemDTO> itemList = new ArrayList<>();
	private ArrayList<BuyItemDTO> buyItemDTOList = new ArrayList<>();

	@SuppressWarnings("unchecked")
	public String execute() {
		String result = ERROR;
		System.out.println("itemId："+itemId);
		System.out.println("itemName："+itemName);
		System.out.println("itemPrice："+itemPrice);
		System.out.println("itemList"+session.get("itemList"));
		BuyItemDAO dao = new BuyItemDAO();
		BuyItemDTO dto2 = dao.getBuyItemInfo(itemId);
		try {
			if (chk == null && itemId == 0) {
				return SUCCESS;
			} else if (chk != null) {
				for (int i=0;chk.length > i;i++) {
					buyItemDTOList.add(dao.getBuyItemInfo(chk[i]));
				}
			}
			if (session.containsKey("itemList")) {
				itemList = (ArrayList<BuyItemDTO>)session.get("itemList");
				int size = itemList.size();
				for(int i=0; i < size; i++) {
					if(itemId == itemList.get(i).getItemId()) {
						BuyItemDTO dto = new BuyItemDTO();
						dto.setItemId(itemId);
						dto.setItemName(itemName);
						dto.setItemPrice(itemPrice+itemList.get(i).getItemPrice());
						itemList.add(dto);
						itemList.remove(i);
						//itemIdでソート
						Collections.sort(itemList,new Comparator<BuyItemDTO>(){
							public int compare(BuyItemDTO obj1, BuyItemDTO obj2) {
								return ((Integer)obj1.getItemId()).compareTo((Integer)obj2.getItemId());
							}
						});
						session.put("itemList", itemList);
						return SUCCESS;
					}
				}
				BuyItemDTO dto = new BuyItemDTO();
				dto.setItemId(itemId);
				dto.setItemName(itemName);
				dto.setItemPrice(itemPrice);
				itemList.add(dto);
			} else {
				BuyItemDTO dto = new BuyItemDTO();
				dto.setItemId(itemId);
				dto.setItemName(itemName);
				dto.setItemPrice(itemPrice);
				itemList.add(dto);
			}
			//itemIdでソート
			Collections.sort(itemList,new Comparator<BuyItemDTO>(){
				public int compare(BuyItemDTO obj1, BuyItemDTO obj2) {
					return ((Integer)obj1.getItemId()).compareTo((Integer)obj2.getItemId());
				}
			});
			session.put("itemList", itemList);
			result = SUCCESS;
		} catch(Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return result;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}

	public ArrayList<BuyItemDTO> getItemList() {
		return this.itemList;
	}

	public void setItemList(ArrayList<BuyItemDTO> itemList) {
		this.itemList = itemList;
	}
	
	public int[] getChk() {
		return chk;
	}

	public void setChk(int[] chk) {
		this.chk = chk;
	}
	
	public ArrayList<Object> getList() {
		return list;
	}

	public void setList(ArrayList<Object> list) {
		this.list = list;
	}
}
