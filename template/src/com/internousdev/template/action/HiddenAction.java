package com.internousdev.template.action;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import com.internousdev.template.dao.HiddenDAO;
import com.internousdev.template.dto.BuyItemDTO;
import com.opensymphony.xwork2.ActionSupport;

public class HiddenAction extends ActionSupport implements SessionAware{
	String result = ERROR;
	private int hoge3 = 0;
	private Map<String, Object> session;
	private List<BuyItemDTO> list;
	private String upmessage;

	private File[] files;
	private String[] filesContentType;
	private String[] filesFileName;

	private File myFile;
	private String myFileContentType;
	private String myFileFileName;


	@Override
	public String execute() {

		//画像アップロード
		if(myFile != null) {
			System.out.println("File：" + myFile);
			upload(myFile);
		} else if (files != null) {
			for (int i=0;i<files.length;i++) {
				System.out.println("File" + (i+1) + "：" + files[i]);
			}
			System.out.println("FileSize：" + files.length);
			upload(files, filesContentType, filesFileName);
		}

		hoge3 = 15;
		//Map<String, Object> session = getSession();
		HiddenDAO dao = new HiddenDAO();
		session.clear();
		try {
			list = dao.select();
		} catch (Exception e) {
			e.printStackTrace();
		}

		result = SUCCESS;
		return result;
	}

	public String upload(File[] files, String[] filesContentType, String[] filesFileName) {
		System.out.println("コンテキストパス1：" + ServletActionContext.getServletContext().getRealPath("Webimg"));
		System.out.println("コンテキストパス2：" + ServletActionContext.getServletContext().getContextPath());

		String outPath = "F:\\GitLab\\template\\template\\WebContent\\img";
		try {
			int i = 0;
			for(File file : files) {
				File destFile = new File(outPath, filesFileName[i]);
				FileUtils.copyFile(file, destFile);
				i++;
			}
			upmessage = "アップロードに成功しました。";
			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			upmessage = "アップロードに失敗しました。";
			result = ERROR;
		}

		return result;
	}

	public String upload(File files) {
		System.out.println("コンテキストパス1：" + ServletActionContext.getServletContext().getRealPath("Webimg"));
		System.out.println("コンテキストパス2：" + ServletActionContext.getServletContext().getContextPath());

		String outPath = "F:\\GitLab\\template\\template\\WebContent\\img";
		try {
			File destFile = new File(outPath, myFileFileName);
			FileUtils.copyFile(myFile, destFile);
			upmessage = "アップロードに成功しました。";
			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			upmessage = "アップロードに失敗しました。";
			result = ERROR;
		}

		return result;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public void setFilesContentType(String[] filesContentType) {
		this.filesContentType = filesContentType;
	}

	public String[] getFilesContentType() {
		return filesContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public String[] getFilesFileName() {
		return filesFileName;
	}

	public void setFilesFileName(String[] filesFileName) {
		this.filesFileName = filesFileName;
	}

	public String getUpmessage() {
		return upmessage;
	}

	public void setUpmessage(String upmessage) {
		this.upmessage = upmessage;
	}

	public File[] getFiles() {
		return this.files;
	}


	public void setFiles(File[] files) {
		this.files = files;
	}


	public List<BuyItemDTO> getList() {
		return this.list;
	}

	public void setList(List<BuyItemDTO> list) {
		this.list = list;
	}


	public int getHoge3() {
		return this.hoge3;
	}

	public Map<String, Object> getSession() {
		return this.session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
