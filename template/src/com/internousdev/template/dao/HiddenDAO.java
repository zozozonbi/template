package com.internousdev.template.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.internousdev.template.dto.BuyItemDTO;
import com.internousdev.template.util.DBConnector;

public class HiddenDAO {
	DBConnector connector = new DBConnector();
	Connection conn = connector.getConnection();

	private String sql;
	List<BuyItemDTO> list = new ArrayList<>();

	public List<BuyItemDTO> select() throws SQLException {
		sql = "SELECT id, item_name, item_price FROM item_info_transaction";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BuyItemDTO dto = new BuyItemDTO();
				dto.setItemId(rs.getInt("id"));
				//System.out.println("DTO1："+dto.getItemId());
				dto.setItemName(rs.getString("item_name"));
				//System.out.println("DTO2："+dto.getItemName());
				dto.setItemPrice(rs.getInt("item_price"));
				//System.out.println("DTO3："+dto.getItemPrice());
				list.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
		return list;
	}

	public List<BuyItemDTO> home(String[] chk) throws SQLException {
		int length = chk.length;
		System.out.println(length);
		sql = "SELECT id, item_name, item_price FROM item_info_transaction WHERE id IN (" + createInSQL(length) + ")";
		PreparedStatement ps = conn.prepareStatement(sql);
		for (int i=0; i < length; i++) {
			ps.setString(i+1,chk[i]);
		}
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			BuyItemDTO dto = new BuyItemDTO();
			dto.setItemId(rs.getInt("id"));
			dto.setItemName(rs.getString("item_name"));
			dto.setItemPrice(rs.getInt("item_price"));
			list.add(dto);
		}
		return list;
	}

	private static String createInSQL(int length) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length;) {
			builder.append("?");
			if (++i < length) {
				builder.append(",");
			}
		}
		return builder.toString();
	}
}
